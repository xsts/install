# The schematics

This is the folder where the Stop Trek Voyager will create the PDF files with the schematics for a route or a group of routes. After generating the file, you can print it. Use a nice color printer and post the schematics on the window of the bus. The riders will be happy to use this piece of information.
