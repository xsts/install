# The pocket guides

A long time ago, transit agencies were printing and even selling a guide with information about their transit network. That era is long gone. Despite the years, I believe it is interesting to see how easy is to create a PDF from a GTFS dataset. The pages are tall and narrow, just the way the pocket guide was at that time in the past.
