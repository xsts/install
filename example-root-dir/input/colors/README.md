# General remarks

Some GTFS data sets don't have specific colors for the routes even if on the official site there are such colors. The default color in such cases would be solid black (RGB hex code  000000). 

I believe in giving a (pseudo)unique color to each route. It makes it alive. This is why XSTS uses a complementary approach. The default colors of the routes in the GTFS dataset can be overwritten (customized). The process is detailed below.

# Customizing the colors

There are some steps to be performed as it follows:

1. Create a folder for the transit agency that bears the same name as the GTFS folder. For example, Montpellier agency is called TAM, but I prefer to use the name of the town. In our case, the folder is named *montpellier*.

GTFS dataset folder: gtfs/eu/fr/dep34/montpellier

Color folder: input/colors/montpellier

2. In the color folder create one or more color files as following:

color.bus.txt
color.nightbus.txt
color.subway.txt
color.train.txt
color.tram.txt

The name of each file gives the type of transit lines to which it applies. OF a file is missing, the colors for those routes will not overwrite the default ones.

3. Each of the color.*line_type*.txt files has the same structure: a CSV file with a one line header and one or more rows for the colors

The header contains the following columns:

- route_short_name
- red_channel
- green_channel
- blue_channel
- color_hex_value

The rows follow the structure of the header. For example, bus route 20 in Paris uses an orange color (FF5A00). There could be a file named: *color.bus.txt* containing the following text:

route_short_name,red_channel,green_channel,blue_channel,color_hex_value

20,255,90,0,FF5A00

Note that the individual channel values are integers (0-255).

There is a separate project for the semi-automatic extraction of the colors, in case you are interested.

