# Our test agency

FTA stands for Fictional Transit Agency. It doesn't exist but it helps to set up XSTS on your computer. There are a bunch of routes: 

- bus
- tram
- subway 
- train
- nightbus

The GTFS dataset comes without colors for the lines. This means the default behavior is to use solid black for every route. But XSTS is clever enough to mix the GTFS information with post-process customization. Change the name of the folder containing this README file and see for yourself.

