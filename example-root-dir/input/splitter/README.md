# Splitting large GTFS datasets

GTFS has been designed for the cloud where you find machines with vast amounts of memory (RAM) and disk space (HDD/SDD). A traditional transit agency or an individual who wants to experiment with GTFS could be easily discouraged by the size of some files (stop_times.txt). In 2019, the average computer (PC)  sold in stores had 4-6 GB of RAM. Today (2021), it has 8GB of memory. It seems to be enough, but it is not. 

The best thing to do is to split a multi-agency GTFS dataset into individual *one-per-agency* sets. The GTFS splitter had been originally hardcoded to use the *tmp* folder. I am planning to make it more flexible and use a CLI-based approach. Expect the splitter to adapt to the new paradigm. 
