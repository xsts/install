These are the continental codes used by XSTS. Feel free to use your own codes.

Africa - af
Asia - as
Europe - eu
North America - na
Oceania (including Australia and New Zealand) - oa
South America (including Carribean countries) - sa
Virtual world used for future enhancements (feel free to adapt it for yourself) - wo
 
