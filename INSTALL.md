# Installing XSTS

You will need to perform two types of tasks:

- preparing the file system
- cloning/downloading the repos

## The file system

XSTS keeps all its configuration in a single place. I intend to make it more flexible. Having a single point of entry is quite a good idea; you always know where to look for information.

The top level folder for XSTS is what I call the *root dir*. On my computer it is the folder *$HOME/projects/xsts*. It doesn't matter where it sits as long as it follows the XSTS guidelines, i.e. has the right folder structure. There are several sub-folders whose names speak for themselves:

- gtfs
- input
- output

## The gtfs folder

The *gtfs* folder is the top level folder for the GTFS datasets. It is a hierarchical file-system following the following format:

gtfs/continent-code/country-code/district-code/town/agency/*.txt

If you are an agency, most likely there will be a single GTFS dataset. However, if you are playing with GTFS datasets, using the approach I mentioned keeps the information clean and easy to maintain.

## The input folder

The GTFS datasets may or may not specify which color to use when drawing the schematis for a  given route.  XSTS can overwrite the default colors. The input folder contains two sub-folders:

- agencies
- colors

The *colors* folder contains per-agency customization.

## The output folder

This is the place  where the PDF files will be genarated. Each java app will create its own sub-folder, avoiding  the mix-in of different types of files.

## Creating the root-dir

The simplest way to  create the top level XSTS folder is to copy the example-root-dir folder where you need it to be. This way, the file structure is already in place. There are additional *README.md* files here and there.

## Cloning the repos

Note: you will need the following pieces of software:

- JDK (>= version 9)
- maven
- git

Choose a place of your choice where you will put the java projects. I suggest to mirror the file structure from gitlab. Use *git* to clone the repositories.

So far, there is one visible java app: the STV generator. Under the hood, it requires other XSTS jar files. 

For every java project, you need to perform a *maven* command: mvn clean package install. This compiles the project and installs it in a common place visible by maven. All the java projects will refer to this maven repository when compiling.

## Order of compilation/install 

There are dependencies between the projects. The one time compile order for every project  is the following:

- xsts/core
- xsts/cli
- xsts/pdf
- xsts/tad/world-registry
- xsts/gtfs/gtfs-gds
- xsts/gtfs/gdl

Now you can compile and run the following apps:

- xsts/gtfs/gtfs-splitter
- xsts/pti/cls/stv

## Using the STV app

Note: you will need a valid GTFS dataset. I suggest to use a small one. Go to the transitfeeds site and download the GTFS for a small town, for example Europe/Serbia/Kragujevac. It is an old dataset, but it works nice. You need to uncompress the .txt files somewhere under the *gtfs* sub-folder.

