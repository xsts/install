# Install

This is a support-project that will help you with the install and configuration of XSTS on your system. If you want to go to the install part, please read the INSTALL.md file.

## Getting started

Welcome to the XSTS install tutorial. If you are here, it means you are interested in public transportation. It is a huge industry with a lot of things to do. Many cities are chasing the personal cars  out of the streets to such extent that car owners are selling their vehicles. Indeed, living in a city is a matter of choice. With a good public transportation system, the need for personal vehicles is close to zero. Most of the transit systems around the world are far from being ideal. They are good, but there is always room for improvment. This is where XSTS enters the stage.

## Scenarios

XSTS is a collection of software tools that help public transit agencies to provide a better ride experience to their users. This includes :

- route schematics to print and post at stops or in buses.
- maps of the network
- timetables for every stop

If you think I missed the apps, you're right. So far, XSTS has skipped the apps for a good reason: absence of hosting. Most of the transit mobile apps that exist are doing a nice job of informing people, if they exist. Developing and maintaining a mobile app is costly. On the other hand, PDF files ready to print are not complicated to maintain at all. 

## Data

XSTS starts from a basic hypothesis: there exists a GTFS dataset for a given transit agency and if there is no such information, it is possible to generate it. I know from personal experience that a word or Excel file can be converted to GTFS in one or two days. Let's say a week for those who start from scratch. So, yeah, I can safely assume that you have the GTFS data.

## Environment

You will need a working Java environment. I chose Java for the simple reason that it works on most computers, be it Windows, Mac or Linux. While you can customize the code or write your own program, XSTS already comes with one application. It should go out of the box. Well, you will need to read the install procedure, but then again, it is quite straightforward.

## Printing

I leave you to decide about the printed schematics. For most of the bus routes, the  schematics should fit nicely on a sheet of paper with the width of an A4.Letter page. As for the length, it depends on the number of stops. Most of the time, a sheet of paper twice the length of an A4/letter page should be enough.
